//
//  Utils.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 24/10/20.
//

import Foundation
import os.log

enum Logs: String {
    case search = "Error searhing product"
    case fetchProductDetail = "Error trying to fetch the product details"
    case sites = "Error fetching the sites"
    case parsingSite = "Error parsin the site"
    case url = "Error appending path to the url"
    case emptyResponse = "Emtpy response for url"
    case parseProducts = "Error parsing products"
    case parseProductDetail = "Error parsing product details"
}

struct Utils {
    func convertPriceToCurrency(_ price: Int) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.maximumFractionDigits = 0
        numberFormatter.locale = Locale.current
        
        var numberString = numberFormatter.string(from: NSNumber(value: price))!
        numberString.insert(" ", at: numberString.index(after: numberString.startIndex))
        return numberString
    }
}

extension Logger {
    private static var bundleId = Bundle.main.bundleIdentifier!
    
    static let repository = Logger(subsystem: bundleId, category: "repository")
    static let sites = Logger(subsystem: bundleId, category: "sites")
    static let products = Logger(subsystem: bundleId, category: "products")
}
