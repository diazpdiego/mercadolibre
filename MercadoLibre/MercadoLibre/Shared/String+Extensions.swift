//
//  String+Extensions.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 24/10/20.
//

import Foundation

extension String {
    var formattedToSearch: String {
        var text = self
        text = text.replacingOccurrences(of: " ", with: "+")
        text = text.replacingOccurrences(of: "á", with: "a")
        text = text.replacingOccurrences(of: "é", with: "e")
        text = text.replacingOccurrences(of: "í", with: "i")
        text = text.replacingOccurrences(of: "ó", with: "o")
        text = text.replacingOccurrences(of: "ú", with: "u")
        
        return text
    }
}
