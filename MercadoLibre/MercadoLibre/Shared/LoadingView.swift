//
//  LoadingView.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 26/10/20.
//

import UIKit

class LoadingView: UIView {

    //MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    //MARK: - Private Methods
    
    private func setupUI() {
        self.backgroundColor = UIColor.black
        self.layer.cornerRadius = 10
        
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.color = .white
        activityIndicator.frame = CGRect(x: 0 , y: 0, width: GlobalConstants.loadingSize.width, height: GlobalConstants.loadingSize.height)
        activityIndicator.startAnimating()
        self.addSubview(activityIndicator)

    }
}
