//
//  PictureCell.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 25/10/20.
//

import UIKit

class PictureCell: UICollectionViewCell {

    //MARK: - Properties
    
    @IBOutlet weak var productImageView: UIImageView!
    
    //MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Public Methods
    
    func configure(with imageUrlString: String) {
        productImageView.setImageFromUrl(imageUrlString, placeHolder: nil)
    }

}
