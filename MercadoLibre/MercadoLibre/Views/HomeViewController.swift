//
//  HomeViewController.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 23/10/20.
//

import UIKit

class HomeViewController: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var siteNameButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private lazy var sitesViewModel = SitesViewModel()
    private lazy var productsViewModel = ProductsViewModel()
    
    private var sites: [SiteModel] = []
    private var sitesPicker = UIPickerView()
    private var toolbar = UIToolbar()
    private var siteId: String {
        get { UserDefaultsManager.siteId }
    }
    
    //MARK: - Config
    
    private enum Config {
        static let productsViewController   = "ProductsViewController"
        static let textColorKey             = "textColor"
        static let searchBarPlaceHolder     = "Buscar en Mercado Libre"
        static let searchFieldKey           = "searchField"
        static let alertNoResultsMessage    = "Tu búsqueda no ha arrojado resultados. Por favor intenta con otras palabras"
        static let title                    = "Búsqueda"
        static let minimumCharacters        = 3
        static let heightPicker             : CGFloat = 300
        static let heightToolBar            : CGFloat = 50
        static let searchBarBgColor         = UIColor(red: 241.0 / 255.0, green: 241.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
    }
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        setupUI()
        fetchSites()
    }
    
    //MARK: - Private Methods
    
    private func setupUI() {
        searchBar.delegate = self
        let textFieldInsideSearchBar = searchBar.value(forKey: Config.searchFieldKey) as? UITextField
        textFieldInsideSearchBar?.textColor = .black
        textFieldInsideSearchBar?.backgroundColor = Config.searchBarBgColor
        textFieldInsideSearchBar?.leftView?.tintColor = .black
        
        title = Config.title
    }
    
    private func fetchSites() {
        sitesViewModel.fetchSites { [weak self] sites in
            let site = sites.first { $0.name == UserDefaultsManager.site } ?? GlobalConstants.defaultSite
            self?.sites = sites
            self?.updateDefaultSite(site)
        }
    }
    
    private func updateDefaultSite(_ site: SiteModel) {
        sitesViewModel.saveDefaultSite(site)
        updateSiteTitle(site.name)
    }
    
    private func updateSiteTitle(_ title: String) {
        DispatchQueue.main.async {
            self.siteNameButton.setTitle(title, for: .normal)
        }
    }
    
    private func searchProduct(_ text: String) {
        productsViewModel.searchProducts(with: text, in: siteId) { [weak self] result in
            switch result {
            case .success(let products):
                guard
                    let productsSearched = products,
                    productsSearched.count > 0 else {
                    self?.hideLoadingView()
                    self?.showAlert(with: GlobalConstants.genericAlertTitle, and: Config.alertNoResultsMessage)
                    return
                }
                
                DispatchQueue.main.async {
                    self?.hideLoadingView()
                    
                    let storyboard = UIStoryboard(name: GlobalConstants.mainStoryboard, bundle: Bundle.main)
                    
                    let productsViewController = storyboard.instantiateViewController(withIdentifier: Config.productsViewController) as! ProductsViewController
                    productsViewController.products = productsSearched
                    productsViewController.title = text
                    
                    self?.navigationController?.pushViewController(productsViewController, animated: true)
                }
            case .failure(let error):
                self?.hideLoadingView()
                self?.showAlert(with: GlobalConstants.genericAlertTitle, and: error.info.message)
            }
        }
    }
    
    //MARK: - Actions
    
    @objc private func dismissPicker() {
        toolbar.removeFromSuperview()
        sitesPicker.removeFromSuperview()
    }
    
    @IBAction func showSitesList(_ sender: UIButton) {
        sitesPicker = UIPickerView.init()
        sitesPicker.delegate = self
        sitesPicker.dataSource = self
        sitesPicker.backgroundColor = UIColor.white
        sitesPicker.setValue(UIColor.black, forKey: Config.textColorKey)
        sitesPicker.autoresizingMask = .flexibleWidth
        sitesPicker.contentMode = .center
        sitesPicker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - Config.heightPicker, width: UIScreen.main.bounds.size.width, height: Config.heightPicker)
        self.view.addSubview(sitesPicker)
        
        toolbar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - Config.heightPicker, width: UIScreen.main.bounds.size.width, height: Config.heightToolBar))
        let doneButton      = UIBarButtonItem(title: GlobalConstants.agree, style: .plain, target: self, action: #selector(dismissPicker));
        let spaceButton     = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton    = UIBarButtonItem(title: GlobalConstants.cancel, style: .plain, target: self, action: #selector(dismissPicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        view.addSubview(toolbar)
    }
}

//MARK: - Picker Delegate and Datasource Methods

extension HomeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sites.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sites[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateDefaultSite(sites[row])
        updateSiteTitle(sites[row].name)
    }
    
}

//MARK: - Search Bar Delegate

extension HomeViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text, text.count >= Config.minimumCharacters {
            showLoadingView()
            searchProduct(text.formattedToSearch)
        }
    }
}
