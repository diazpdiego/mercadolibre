//
//  ProductDetailViewController.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 24/10/20.
//

import UIKit

class ProductDetailViewController: UIViewController {

    //MARK: - Config
    
    private enum Config {
        static let soldKey                          = "vendidos"
        static let availableStock                   = "Stock disponible"
        static let unavailableStock                 = "No tenemos Stock disponible"
        static let emptyResponse                    = "--"
        static let emptyString                      = ""
        static let pictureCellName                  = "PictureCell"
        static let attributeCellId                  = "attributeCell"
        static let originalPriceHeightConstraint    : CGFloat = 24
        static let freeShippingHeightConstraint     : CGFloat = 30
        static let ceroHeightConstraint             : CGFloat = 0.0
    }
    
    //MARK: - Properties
    
    @IBOutlet weak var soldLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var originalPriceLabel: UILabel!
    @IBOutlet weak var newPriceLabel: UILabel!
    @IBOutlet weak var availableStockLabel: UILabel!
    @IBOutlet weak var storeWarrantyLabel: UILabel!
    @IBOutlet weak var freeShippingLabel: UILabel!
    @IBOutlet weak var freeShippingHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var originalPriceHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var picturesCollectionView: UICollectionView!
    @IBOutlet weak var attributesTableView: UITableView!
    
    var product: ProductModel?
    private let utils = Utils()
    private let flowLayout = UICollectionViewFlowLayout()
    private var productImages : [Picture] = []
    private var attibutes: [Attribute] = []
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    //MARK: - Private Methods
    
    private func setupUI() {
        guard let productDetail = product else { return }
        
        picturesCollectionView.register(UINib(nibName: Config.pictureCellName, bundle: nil), forCellWithReuseIdentifier: Config.pictureCellName)
        flowLayout.scrollDirection = .horizontal
        picturesCollectionView.collectionViewLayout = flowLayout

        if let pictures = productDetail.pictures {
            productImages = pictures
        }
        
        soldLabel.text = "\(productDetail.soldQuantity) \(Config.soldKey)"
        titleLabel.text = productDetail.title
        
        if let originalPrice = productDetail.originalPrice {
            originalPriceHeightConstraint.constant = Config.originalPriceHeightConstraint
            
            let priceCurrency = utils.convertPriceToCurrency(originalPrice)
            
            let originalPriceString: NSMutableAttributedString =  NSMutableAttributedString(string: priceCurrency)
            originalPriceString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0, originalPriceString.length))
            originalPriceLabel.attributedText = originalPriceString
            
        } else {
            originalPriceHeightConstraint.constant = Config.ceroHeightConstraint
        }
        
        freeShippingHeightConstraint.constant = productDetail.shipping.freeShipping ? Config.freeShippingHeightConstraint : Config.ceroHeightConstraint
        freeShippingLabel.text = productDetail.shipping.freeShipping ? GlobalConstants.freeShipping : Config.emptyString
        newPriceLabel.text = utils.convertPriceToCurrency(productDetail.price)
        availableStockLabel.text = productDetail.availableQuantity >= 1 ? Config.availableStock : Config.unavailableStock
        storeWarrantyLabel.text = productDetail.warranty ?? Config.emptyResponse        
        product = nil
        
        if let features = productDetail.attributes {
            attibutes = features
        }
        
        attributesTableView.delegate = self
        attributesTableView.dataSource = self
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:GlobalConstants.emptyString, style:.plain, target:nil, action:nil)
    }
}

//MARK: - CollectionView Delegate Methods

extension ProductDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Config.pictureCellName, for: indexPath) as! PictureCell
        cell.configure(with: productImages[indexPath.row].secureUrl)
        return cell
    }
}

//MARK: - CollectionView Flowlayout Delegate Methods

extension ProductDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: picturesCollectionView.frame.width, height: picturesCollectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0.0
    }
}

//MARK: - TableView Delegate Methods

extension ProductDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attibutes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Config.attributeCellId)!
        cell.textLabel?.text = attibutes[indexPath.row].name
        cell.detailTextLabel?.text = attibutes[indexPath.row].value
        
        return cell
    }
}
