//
//  SiteModel.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 23/10/20.
//

import Foundation

struct SiteModel: Decodable {
    var id: String
    var name: String
}
