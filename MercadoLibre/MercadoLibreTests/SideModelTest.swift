//
//  SideModelTest.swift
//  MercadoLibreTests
//
//  Created by Diego Diaz on 27/10/20.
//

import XCTest
@testable import MercadoLibre

class SideModelTests: XCTestCase {
    
    private enum Config {
        static let id = "MBO"
        static let name = "Bolivia"
        static let failureSitejson : [String: String] = [
            "id": "MCO",
            "name": "Colombia"
        ]
        static let successSiteJson : [[String: String]] = [
            [
                "id": "MCO",
                "name": "Colombia"
            ],
            [
                "id": "MLA",
                "name": "Argentina"
            ]
        ]
    }
    
    var site: SiteModel!
    
    override func setUp() {
        super.setUp()
        
        site = SiteModel(id: Config.id, name: Config.name)
    }
    
    override func tearDown() {
        super.tearDown()
        
        site = nil
    }

    
    func testFail() {
        let decoder = JSONDecoder()
        let encoder = JSONEncoder()
        
        let data = try? encoder.encode(Config.failureSitejson)
        let siteModel = try? decoder.decode([SiteModel].self, from: data!)
        XCTAssertNil(siteModel)
    }
    
    func testSiteSuccess() {
        let decoder = JSONDecoder()
        let encoder = JSONEncoder()
        
        let data = try? encoder.encode(Config.successSiteJson)
        let siteModel = try? decoder.decode([SiteModel].self, from: data!)
        XCTAssertNotNil(siteModel)
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

}
