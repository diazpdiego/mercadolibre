//
//  XCTestCase+Extensions.swift
//  MercadoLibreTests
//
//  Created by Diego Diaz on 27/10/20.
//

import XCTest

typealias AsynchronousTest = (_ fulfill: @escaping () -> Void) -> Void

extension XCTestCase {
    func runAsynchronousTest(withTimeout timeout: TimeInterval, test: AsynchronousTest) {
        let completionExpectation = expectation(description: "runAsynchronousTest")
        
        test {
            completionExpectation.fulfill()
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
}
