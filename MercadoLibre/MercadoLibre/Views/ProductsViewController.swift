//
//  ProductsViewController.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 24/10/20.
//

import UIKit

class ProductsViewController: UIViewController {
    
    //MARK: - Config
    
    private enum Config {
        static let productCellId                = "ProductCell"
        static let productCellNibName           = "ProductCell"
        static let productDetailViewController  = "ProductDetailViewController"
    }
    
    //MARK: - Properties
    
    @IBOutlet weak var productsTableView: UITableView!
    
    var products: [ProductModel] = []
    private lazy var productsViewModel = ProductsViewModel()

    //MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: - Private Methods
    
    private func setupUI() {
        view.backgroundColor = .white
        
        productsTableView.delegate = self
        productsTableView.dataSource = self
        
        let nib = UINib(nibName: Config.productCellNibName, bundle: nil)
        productsTableView.register(nib, forCellReuseIdentifier: Config.productCellId)
        
        navigationController?.navigationBar.tintColor = .black
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:GlobalConstants.emptyString, style:.plain, target:nil, action:nil)
    }
}

//MARK: - TableView Delegates

extension ProductsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductCell = tableView.dequeueReusableCell(withIdentifier: Config.productCellId) as! ProductCell
        cell.configure(with: products[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showLoadingView()

        productsViewModel.fetchProductDetail(products[indexPath.row].id) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let product):
                DispatchQueue.main.async {
                    strongSelf.hideLoadingView()

                    let storyboard = UIStoryboard(name: GlobalConstants.mainStoryboard, bundle: Bundle.main)

                    let productDetailVC = storyboard.instantiateViewController(withIdentifier: Config.productDetailViewController) as! ProductDetailViewController
                    productDetailVC.product = product
                    
                    strongSelf.navigationController?.pushViewController(productDetailVC, animated: true)
                }
            
            case .failure(let error):
                strongSelf.showAlert(with: GlobalConstants.genericAlertTitle, and: error.info.message)
                break
            }
        }
    }
    
}
