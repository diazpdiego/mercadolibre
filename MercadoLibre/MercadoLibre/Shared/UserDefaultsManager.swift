//
//  UserDefaultsManager.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 23/10/20.
//

import Foundation

struct UserDefaultsManager {
    
    //MARK: - Keys
    
    enum Key: String {
        case siteName
        case siteId
    }
    
    // MARK: - Properties
    
    private static var userDefaults = UserDefaults.standard
    
    static var site: String {
        get {
            return userDefaults.string(forKey: Key.siteName.rawValue) ?? GlobalConstants.defaultSite.name
        }
        set {
            userDefaults.set(newValue, forKey: Key.siteName.rawValue)
        }
    }
    
    static var siteId: String {
        get {
            return userDefaults.string(forKey: Key.siteId.rawValue) ?? GlobalConstants.defaultSite.id
        }
        set {
            userDefaults.set(newValue, forKey: Key.siteId.rawValue)
        }
    }
}

