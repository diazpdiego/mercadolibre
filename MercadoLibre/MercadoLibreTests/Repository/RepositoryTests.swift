//
//  RepositoryTests.swift
//  MercadoLibreTests
//
//  Created by Diego Diaz on 27/10/20.
//

import XCTest
@testable import MercadoLibre

class ProductsViewModelTests: XCTestCase {

    private enum Config {
        static let notExistingProduct   = "blaqweakjdqwyeq"
        static let notExistingSite      = "HTUIO"
        static let existingProduct      = "Dron"
        static let existingSite         = "MCO"
        static let timeOut: TimeInterval = 30
    }
       
    var repository: RepositoryProtocol?
    
    override func setUp() {
        super.setUp()
        
        repository = Repository.shared
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFailSearchProducts() {
        if let repo = repository {
            runAsynchronousTest(withTimeout: Config.timeOut) { fullfill in
                repo.getProducts(for: Config.notExistingProduct, in: Config.notExistingSite) { data in
                    print("la data: \(data)")
                    XCTAssertNotNil(data)
                    
                    fullfill()
                }
            }
        }
    }
    
//    func testSiteSuccess() {
//        let decoder = JSONDecoder()
//        let encoder = JSONEncoder()
//
//        let data = try? encoder.encode(Config.successSiteJson)
//        let siteModel = try? decoder.decode([SiteModel].self, from: data!)
//        XCTAssertNotNil(siteModel)
//    }
}
