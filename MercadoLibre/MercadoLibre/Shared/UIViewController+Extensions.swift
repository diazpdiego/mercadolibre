//
//  UIViewController+Extensions.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 23/10/20.
//

import UIKit

var loadingView = LoadingView()

extension UIViewController {

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showLoadingView() {
        DispatchQueue.main.async {
            loadingView = LoadingView(frame: CGRect(x: self.view.frame.midX - (GlobalConstants.loadingSize.width / 2) , y: self.view.frame.midY - (GlobalConstants.loadingSize.height / 2), width: GlobalConstants.loadingSize.width, height: GlobalConstants.loadingSize.height))
            self.view.addSubview(loadingView)
        }
    }
    
    func hideLoadingView() {
        DispatchQueue.main.async {
            loadingView.removeFromSuperview()
        }
    }

    func showAlert(with title: String, and message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let agreeAction = UIAlertAction(title: GlobalConstants.agree, style: .default, handler: nil)

            alert.addAction(agreeAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
