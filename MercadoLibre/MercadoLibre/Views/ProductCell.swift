//
//  ProductCell.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 24/10/20.
//

import UIKit

class ProductCell: UITableViewCell {
    
    //MARK: - Config
    
    private enum Config {
        static let availableQuantity = "Disponible:"
    }
    
    //MARK: - Properties
    
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!
    @IBOutlet weak var availableQuantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    private lazy var utils = Utils()
    
    //MARK: Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with product: ProductModel) {
        titleLabel.text = product.title
        thumbnail.setImageFromUrl(product.thumbnail, placeHolder: nil)
        shippingLabel.text = product.shipping.freeShipping ? GlobalConstants.freeShipping : ""
        availableQuantityLabel.text = "\(Config.availableQuantity) \(product.availableQuantity)"
        priceLabel.text = utils.convertPriceToCurrency(product.price)
    }
    
    
}
