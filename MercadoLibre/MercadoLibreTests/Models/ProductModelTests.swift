//
//  ProductModelTest.swift
//  MercadoLibreTests
//
//  Created by Diego Diaz on 27/10/20.
//

import XCTest
@testable import MercadoLibre

class ProductModelTests: XCTestCase {
    
    private enum Config {
        static let failureProductjson : [String: Any] = [
            "id": "MLA800260685",
            "site_id": "MLA",
            "title": "Moto G6 Plus 64 Gb Índigo Oscuro 4 Gb Ram",
            "price": 40600,
            "available_quantity": 999,
            "sold_quantity": 10,
            "condition": "new",
            "thumbnail": "http://http2.mlstatic.com/D_909111-MLA31239994076_062019-I.jpg",
            "shipping": [
                "free_shipping": true
            ],
            "attributes": [
                [
                    "value_name": "Motorola",
                    "name": "Marca"
                ],
                [
                    "value_name": "Adreno 508",
                    "name": "Modelo de GPU"
                ],
                [
                    "name": "Condición del ítem",
                    "value_name": "Nuevo"
                ]
            ]
        ]
        static let successProductsJson : [[String: Any]] = [
            [
                "id": "MLA800260685",
                "site_id": "MLA",
                "title": "Moto G6 Plus 64 Gb Índigo Oscuro 4 Gb Ram",
                "price": 40600,
                "available_quantity": 999,
                "sold_quantity": 10,
                "condition": "new",
                "thumbnail": "http://http2.mlstatic.com/D_909111-MLA31239994076_062019-I.jpg",
                "shipping": [
                    "free_shipping": true
                ],
                "attributes": [
                    [
                        "value_name": "Motorola",
                        "name": "Marca"
                    ],
                    [
                        "value_name": "Adreno 508",
                        "name": "Modelo de GPU"
                    ],
                    [
                        "name": "Condición del ítem",
                        "value_name": "Nuevo"
                    ]
                ]
            ],
            [
                "id": "MLA800260685",
                "site_id": "MLA",
                "title": "Moto G6 32 Gb Índigo Oscuro 3 Gb Ram",
                "price": 32600,
                "available_quantity": 999,
                "sold_quantity": 1,
                "condition": "new",
                "thumbnail": "http://http2.mlstatic.com/D_632562-MLA31003470563_062019-I.jpg",
                "shipping": [
                    "free_shipping": true
                ],
                "attributes": [
                    [
                        "value_name": "Motorola",
                        "name": "Marca"
                    ],
                    [
                        "value_name": "Condición del ítem",
                        "name": "Nuevo"
                    ],
                    [
                        "name": "Linea",
                        "value_name": "Moto G"
                    ]
                ]
            ]
        ]
    }
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func testProductsFail() {
        let decoder = JSONDecoder()
        
        let data = try? JSONSerialization.data(withJSONObject: Config.failureProductjson, options: .prettyPrinted)
        let siteModel = try? decoder.decode([ProductModel].self, from: data!)
        XCTAssertNil(siteModel)
    }
    
    func testProductsSuccess() {
        let decoder = JSONDecoder()
        
        let data = try? JSONSerialization.data(withJSONObject: Config.successProductsJson, options: .prettyPrinted)
        let siteModel = try? decoder.decode([ProductModel].self, from: data!)
        XCTAssertNotNil(siteModel)
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
}
