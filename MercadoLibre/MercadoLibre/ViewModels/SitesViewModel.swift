//
//  SitesViewModel.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 23/10/20.
//

import Foundation
import os.log

class SitesViewModel {
    
    //MARK: - Config
    
    private enum Config {
        static let defaultSite = [GlobalConstants.defaultSite]
    }
    
    //MARK: - Properties
    
    var repository : RepositoryProtocol?
    
    //MARK: - Initializers
    
    init(repository: RepositoryProtocol = Repository.shared) {
        self.repository = repository
    }
    
    //MARK: - Public Methods
    
    func fetchSites(completion: @escaping (_ sites: [SiteModel]) -> Void) {
        if let repository = repository {
            repository.getSites { data in
                guard let sitesData = data else {
                    Logger.sites.error("\(Logs.sites.rawValue)")
                    completion(Config.defaultSite)
                    return
                }
                
                let sites = self.parseSites(sitesData)
                completion(sites)
            }
        }
    }
    
    func saveDefaultSite(_ site: SiteModel) {
        UserDefaultsManager.site = site.name
        UserDefaultsManager.siteId = site.id
    }
    
    //MARK: - Private Methods
    
    private func parseSites(_ data: Data) -> [SiteModel] {
        let decoder = JSONDecoder()
        
        guard let siteModel = try? decoder.decode([SiteModel].self, from: data) else {
            Logger.sites.error("\(Logs.parsingSite.rawValue)")
            return Config.defaultSite
        }
        
        return siteModel
    }
}
