//
//  LocationManager.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 23/10/20.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    
    //MARK: - Properties
    
    private var locationManager: CLLocationManager?
    private lazy var geocoder = CLGeocoder()
    
    //MARK: - Private Methods

    private func reverseGeocode(location: CLLocation) {
        geocoder.reverseGeocodeLocation(location) { [weak self] (placemarks, error) in
            self?.parseLocation(with: placemarks, error: error)
        }
    }
    
    private func parseLocation(with placemarks: [CLPlacemark]?, error: Error?) {
        guard
            let userPlacemarks = placemarks,
            let placemark = userPlacemarks.first else {
            setDefaultSite()
            return
        }
        
        UserDefaultsManager.site = placemark.country ?? GlobalConstants.defaultSite.name
    }
    
    private func setDefaultSite() {
        UserDefaultsManager.site = GlobalConstants.defaultSite.name
        UserDefaultsManager.siteId = GlobalConstants.defaultSite.id
    }
    
    //MARK: - Public Methods
    
    func getLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
    }
}

//MARK: - CLLocationManagerDelegate

extension LocationManager: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    if status == .authorizedAlways || status == .authorizedWhenInUse {
        locationManager?.startUpdatingLocation()
    } else {
        setDefaultSite()
    }
  }
      
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.last else { return }
      
    locationManager?.stopUpdatingLocation()
    reverseGeocode(location: location)
  }
}
