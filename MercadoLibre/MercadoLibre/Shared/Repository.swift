//
//  Repository.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 23/10/20.
//

import Foundation
import os.log

typealias requestCompletionHandler = (_ data: Data?) -> ()

protocol RepositoryProtocol {
    func getSites(completion: @escaping requestCompletionHandler)
    func getProducts(for text: String, in site: String, completion: @escaping requestCompletionHandler)
    func getProductDetail(_ productId: String, completion: @escaping requestCompletionHandler)
}

class Repository: RepositoryProtocol {
    
    //MARK: - Configs
    
    private enum EndPoints {
        static let baseUrl              = "https://api.mercadolibre.com/"
        static let sitesPath            = "sites"
        static let searchPath           = "sites/%@/search?q=%@"
        static let productDetailPath    = "items/"
    }
    
    private enum RequestType {
        static let httpGetMethodKey    = "GET"
        static let applicationJsonKey  = "application/json"
        static let contentTypeKey      = "Content-Type"
    }
    
    static let shared = Repository()
    
    //MARK: - Public Methods
    
    func getSites(completion: @escaping requestCompletionHandler) {
        guard let url = URL(string: EndPoints.baseUrl + EndPoints.sitesPath) else {
            Logger.repository.error("\(Logs.url.rawValue) \(EndPoints.sitesPath)")
            completion(nil)
            return
        }
        
        let request = makeUrlRequestWithUrl(url: url)
        sendRequest(with: request) { (data) in
            guard let dataResponse = data else {
                completion(nil)
                return
            }
            
            completion(dataResponse)
        }
    }
    
    func getProducts(for text: String, in site: String, completion: @escaping requestCompletionHandler) {
        let path = String(format: EndPoints.searchPath, site, text)
        
        guard let url = URL(string: EndPoints.baseUrl + path) else {
            Logger.repository.error("\(Logs.url.rawValue) \(path)")
            completion(nil)
            return
        }
        
        let request = makeUrlRequestWithUrl(url: url)
        sendRequest(with: request) { (data) in
            guard let dataResponse = data else {
                completion(nil)
                return
            }
            
            completion(dataResponse)
        }
    }
    
    func getProductDetail(_ productId: String, completion: @escaping requestCompletionHandler) {
        let path = "\(EndPoints.productDetailPath)\(productId)"
        
        guard let url = URL(string: EndPoints.baseUrl + path) else {
            Logger.repository.error("\(Logs.url.rawValue) \(path)")
            completion(nil)
            return
        }
        
        let request = makeUrlRequestWithUrl(url: url)
        sendRequest(with: request) { (data) in
            guard let dataResponse = data else {
                completion(nil)
                return
            }
            
            completion(dataResponse)
        }
    }
    
    //MARK: - Private Methods
    
    private func makeUrlRequestWithUrl(url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = RequestType.httpGetMethodKey
        request.setValue(RequestType.applicationJsonKey, forHTTPHeaderField: RequestType.contentTypeKey)
        
        return request
    }
    
    private func sendRequest(with request: URLRequest, withCompletion completion: @escaping requestCompletionHandler) {
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let dataResponse = data else {
                if let urlString = request.url?.absoluteString {
                    Logger.repository.error("\(Logs.emptyResponse.rawValue) \(urlString)")
                }
                
                completion(nil)
                return
            }
            completion(dataResponse)
        })
        task.resume()
    }
}
