//
//  RepositoryTests.swift
//  MercadoLibreTests
//
//  Created by Diego Diaz on 27/10/20.
//

import XCTest
@testable import MercadoLibre

class ProductsViewModelTests: XCTestCase {

    private enum Config {
        static let notExistingProduct       = "blaqweakjdqwyeq"
        static let notExistingSite          = "HTUIO"
        static let existingProduct          = "Dron"
        static let existingSite             = "MCO"
        static let notExistingProductId     = "MCO560478243abc"
        static let existingProductId        = "MCO560478243"
        static let timeOut: TimeInterval    = 20
    }
       
    var viewModel = ProductsViewModel()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFailSearchProducts() {
        runAsynchronousTest(withTimeout: Config.timeOut) { fullfill in
            viewModel.searchProducts(with: Config.notExistingProduct, in: Config.notExistingSite) { result in
                switch result {
                case .success(let products):
                    if let _ = products {
                        XCTFail()
                    } else {
                        XCTAssertNil(products)
                    }
                    
                case .failure:
                    XCTAssertTrue(true)
                }
                
                fullfill()
            }
        }
    }
    
    func testSuccessSearchProducts() {
        runAsynchronousTest(withTimeout: Config.timeOut) { fullfill in
            viewModel.searchProducts(with: Config.existingProduct, in: Config.existingSite) { result in
                switch result {
                case .success(let products):
                    if let productsSearched = products {
                        XCTAssertNotNil(productsSearched)
                    } else {
                        XCTFail()
                    }
                    
                case .failure:
                    XCTFail()
                }
                
                fullfill()
            }
        }
    }
    
    func testFailFetchProductDetail() {
        runAsynchronousTest(withTimeout: Config.timeOut) { fullfill in
            viewModel.fetchProductDetail(Config.notExistingProductId) { result in
                switch result {
                case .success(let product):
                    if let _ = product {
                        XCTFail()
                    } else {
                        XCTAssertNil(product)
                    }
                case .failure:
                    XCTAssertTrue(true)
                }
                
                fullfill()
            }
        }
    }
    
    func testSuccessFetchProductDetail() {
        runAsynchronousTest(withTimeout: Config.timeOut) { fullfill in
            viewModel.fetchProductDetail(Config.existingProductId) { result in
                switch result {
                case .success(let product):
                    if let _ = product {
                        XCTAssertNotNil(product)
                    } else {
                        XCTFail()
                    }
                case .failure:
                    XCTFail()
                }
                
                fullfill()
            }
        }
    }
}
