//
//  SideModelTest.swift
//  MercadoLibreTests
//
//  Created by Diego Diaz on 27/10/20.
//

import XCTest
@testable import MercadoLibre

class SideModelTests: XCTestCase {
    
    private enum Config {
        static let failureSitejson : [String: String] = [
            "id": "MCO",
            "name": "Colombia"
        ]
        static let successSiteJson : [[String: String]] = [
            [
                "id": "MCO",
                "name": "Colombia"
            ],
            [
                "id": "MLA",
                "name": "Argentina"
            ]
        ]
    }
        
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFail() {
        let decoder = JSONDecoder()
        
        let data = try? JSONSerialization.data(withJSONObject: Config.failureSitejson, options: .prettyPrinted)
        let siteModel = try? decoder.decode([SiteModel].self, from: data!)
        XCTAssertNil(siteModel)
    }
    
    func testSiteSuccess() {
        let decoder = JSONDecoder()
        let encoder = JSONEncoder()
        
        let data = try? encoder.encode(Config.successSiteJson)
        let siteModel = try? decoder.decode([SiteModel].self, from: data!)
        XCTAssertNotNil(siteModel)
    }
}
