//
//  GlobalConstants.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 23/10/20.
//

import UIKit

final class GlobalConstants: NSObject {
    static let defaultSite = SiteModel(id: "MCO", name: "Colombia")
    static let agree = "Aceptar"
    static let cancel = "Cancelar"
    static let freeShipping = "Envío gratis"
    static let mainStoryboard = "Main"
    static let genericAlertTitle = "Información"
    static let emptyString = " "
    static let loadingSize : CGSize = CGSize(width: 100, height: 100)
}
