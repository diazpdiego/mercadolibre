//
//  ProductModel.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 23/10/20.
//

import Foundation

struct ProductsResponse: Decodable {
    var results: [ProductModel]
}

struct ProductModel: Decodable {
    var id: String
    var title: String
    var price: Int
    var originalPrice: Int?
    var availableQuantity: Int
    var soldQuantity: Int
    var condition: String
    var thumbnail: String
    var shipping: Shipping
    var attributes: [Attribute]?
    var pictures: [Picture]?
    var warranty: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case price
        case originalPrice = "original_price"
        case availableQuantity = "available_quantity"
        case soldQuantity = "sold_quantity"
        case condition
        case thumbnail
        case shipping
        case attributes
        case pictures
        case warranty
    }
}

struct Shipping: Decodable {
    var freeShipping: Bool
    
    private enum CodingKeys: String, CodingKey {
        case freeShipping = "free_shipping"
    }
}

struct Attribute: Decodable {
    var name: String?
    var value: String?
    
    private enum CodingKeys: String, CodingKey {
        case name
        case value = "value_name"
    }
}

struct Picture: Decodable {
    var id: String
    var url: String
    var secureUrl: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case url
        case secureUrl = "secure_url"
    }
}
