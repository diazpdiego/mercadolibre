//
//  ProductsViewModel.swift
//  MercadoLibre
//
//  Created by Diego Diaz on 24/10/20.
//

import Foundation
import os.log

enum Result<T, Error: Swift.Error> {
    case success(T)
    case failure(Error)
}

enum ProductError: Error {
    case unknowError
    case productNotExist
    
    var info: ErrorDescription {
        switch self {
        case .unknowError:
            return ErrorDescription(message: "Ha ocurrido un error inesperado", code: "ERR001")
        case .productNotExist:
            return ErrorDescription(message: "El producto no existe", code: "ERR002")
        }
    }
}

struct ErrorDescription {
    let message: String
    let code: String
}

typealias productsCompletion = (Result<[ProductModel]?, ProductError>) -> ()
typealias productDetailCompletion = (Result<ProductModel?, ProductError>) -> ()

class ProductsViewModel {
    
    //MARK: - Properties
    
    var repository : RepositoryProtocol?
    
    //MARK: - Initializers
    
    init(repository: RepositoryProtocol = Repository.shared) {
        self.repository = repository
    }
    
    //MARK: - Public Methods
    
    func searchProducts(with text: String, in site: String, completion: @escaping productsCompletion) {
        if let repository = repository {
            repository.getProducts(for: text, in: site) { data in
                guard let productData = data else {
                    Logger.products.error("\(Logs.search.rawValue) \(text)")
                    completion(.failure(.productNotExist))
                    return
                }
                
                let products = self.parseProductsResponse(productData)
                completion(.success(products))
            }
        }
    }
    
    func fetchProductDetail(_ productId: String, completion: @escaping productDetailCompletion) {
        if let repository = repository {
            repository.getProductDetail(productId) { data in
                guard let productData = data else {
                    Logger.products.error("\(Logs.fetchProductDetail.rawValue) \(productId)")
                    completion(.failure(.unknowError))
                    return
                }
                
                let product = self.parseProductDetail(productData)
                completion(.success(product))
            }
        }
    }
    
    //MARK: - Private Methods
    
    private func parseProductsResponse(_ data: Data) -> [ProductModel]? {
        let decoder = JSONDecoder()
        
        guard let productsResponse = try? decoder.decode(ProductsResponse.self, from: data)  else {
            Logger.products.error("\(Logs.parseProducts.rawValue)")
            return nil
        }
        
        return productsResponse.results
    }
    
    private func parseProductDetail(_ data: Data) -> ProductModel? {
        let decoder = JSONDecoder()
        
        guard let productModel = try? decoder.decode(ProductModel.self, from: data)  else {
            Logger.products.error("\(Logs.parseProductDetail.rawValue)")
            return nil
        }
        
        return productModel
    }
}
